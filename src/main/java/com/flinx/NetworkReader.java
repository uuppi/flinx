package com.flinx;

import org.apache.commons.io.IOUtils;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


/**
 *
 * @author Mikko Holopainen
 */
public abstract class NetworkReader 
{
    public static String parseHttpErrorCode(String exception)
    {
        Pattern pattern = Pattern.compile("^java.io.IOException: Server returned HTTP response code: (\\d{3}) for URL");
        Matcher matcher = pattern.matcher(exception);
        if (matcher.find()) return matcher.group(1);
        else return "";
    }
    
    public static String readUrlAsString(String url, int wait) throws Exception
    {
        InputStreamReader isReader = null;
        BufferedReader buffReader = null;
        try
        {
            if(wait > 0) Thread.sleep(wait);
            
            isReader = new InputStreamReader(new URL(url).openStream(), StandardCharsets.UTF_8);
            buffReader = new BufferedReader(isReader);

            return IOUtils.toString(buffReader);
        }
        catch(java.io.IOException e)
        {
            // Mik�li tulee vastaus "429 Too Many Requests", odotetaan hetki ennen pyynn�n uusintaa.
            if(wait == 0 && parseHttpErrorCode(e.toString()).equals("429")) {
                return readUrlAsString(url, 5000);
            }
            else {
                return null;
            }
        }
        finally
        {
            if(buffReader != null) buffReader.close();
            if(isReader != null) isReader.close();
        }
    }
    
    public static String readUrlAsString(String url) throws Exception
    {
        return readUrlAsString(url, 0);
    }

}
