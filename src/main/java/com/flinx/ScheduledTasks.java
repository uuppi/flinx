package com.flinx;

import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

/**
 * @author Urho Laukkarinen
 */
@Component
public class ScheduledTasks
{
    public static final long REFRESH_INTERVAL = 3600000;

    @Scheduled(fixedRate = REFRESH_INTERVAL)
    public void sayHello()
    {
        System.out.println("Hello, World!");
    }
}
