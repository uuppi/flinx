package com.flinx;

import com.net.codeusa.NetflixRoulette;

/**
 *
 * @author Mikko Holopainen
 */
public class DataHandler
{
    private NetflixRoulette netflixRoulette;
    private TmdbDataHandler tmdbDataHandler;
    private CinemaDataHandler cinemaDataHandler;
    
    private FilmList films;
    
    
    public DataHandler()
    {
        netflixRoulette = new NetflixRoulette();
        tmdbDataHandler = new TmdbDataHandler();
        cinemaDataHandler = new CinemaDataHandler();
        
        films = new FilmList();
    }
    
    
    public void loadDataFromDatabase()
    {
        // Tietokannan lukua tähän
    }
    
    public void loadDataFromApis()
    {
        cinemaDataHandler.readCinemasFromApi();
        tmdbDataHandler.loadGenresFromApi();
    }
    
    
    public void updateNetflixStatus(Film film)
    {
        try
        {
            String netflixId = netflixRoulette.getNetflixId(film.getOriginalTitle(), film.getProductionYear());
            film.updateNetflixId(netflixId);
        }
        catch(Exception e)
        {
            film.updateNetflixId(null);
        }
    }
    
    public void updateNetflixStatus(FilmList filmList)
    {
        for(Film film : filmList)
        {
            updateNetflixStatus(film);
        }
    }
    
    public FilmList discover(DiscoverBy discoverBy)
    {
        return discover(discoverBy, null);
    }
    
    public FilmList discover(DiscoverBy discoverBy, Genre genre)
    {
        FilmList discovered = tmdbDataHandler.discover(discoverBy, genre);
        films.add(discovered);
        return discovered;
    }

    public void updateCinemas()
    {
        FilmList filmsInCinemas = cinemaDataHandler.loadFilmsInCinemas();
        tmdbDataHandler.updateFilms(filmsInCinemas);
        cinemaDataHandler.updateCinemas(filmsInCinemas);
    }
    

    public FilmList getFilmList()
    {
        return films;
    }

    public void setFilms(FilmList films)
    {
        this.films = films;
    }

    public NetflixRoulette getNetflixRoulette()
    {
        return netflixRoulette;
    }

    public void setNetflixRoulette(NetflixRoulette netflixRoulette)
    {
        this.netflixRoulette = netflixRoulette;
    }

    public TmdbDataHandler getTmdbDataHandler()
    {
        return tmdbDataHandler;
    }

    public void setTmdbDataHandler(TmdbDataHandler tmdbDataHandler)
    {
        this.tmdbDataHandler = tmdbDataHandler;
    }

    public CinemaDataHandler getCinemaDataHandler()
    {
        return cinemaDataHandler;
    }

    public void setCinemaDataHandler(CinemaDataHandler cinemaDataHandler)
    {
        this.cinemaDataHandler = cinemaDataHandler;
    }
    
    
    
    
    // Testaamiseen
    public static void main(String args[])
    {
        DataHandler dh = new DataHandler();
        dh.loadDataFromApis();
        
        for(Genre genre : dh.getTmdbDataHandler().getGenres())
        {
            FilmList genreFilms = dh.discover(DiscoverBy.VOTE_AVERAGE_BEST, genre);
            dh.updateNetflixStatus(genreFilms);
            System.out.println("-----" + genre.getName() + "-----");
            System.out.println(genreFilms);
        }
        
        dh.updateCinemas();
        
        /*
        dh.updateCinemaProgram();
        
        for(Film film : dh.getFilmList())
        {
            System.out.println(film);
        }

        for(Cinema cinema : dh.getCinemas())
        {
            System.out.println(cinema);
        }
        */
        
        /*
        FilmList popularFilms = dh.discover(DiscoverBy.POPULARITY);
        FilmList oldFilms = dh.discover(DiscoverBy.PRODUCTION_YEAR_OLDEST);
        FilmList newFilms = dh.discover(DiscoverBy.PRODUCTION_YEAR_NEWEST);
        FilmList blockbusterFilms = dh.discover(DiscoverBy.REVENUE);
        FilmList goodFilms = dh.discover(DiscoverBy.VOTE_AVERAGE_BEST);
        FilmList badFilms = dh.discover(DiscoverBy.VOTE_AVERAGE_WORST);

        System.out.println("---SUOSITUT ELOKUVAT---");
        System.out.println(popularFilms);
        System.out.println("----VANHAT ELOKUVAT----");
        System.out.println(oldFilms);
        System.out.println("----UUDET ELOKUVAT-----");
        System.out.println(newFilms);
        System.out.println("--TUOTTOISAT ELOKUVAT--");
        System.out.println(blockbusterFilms);
        */
    }
}
