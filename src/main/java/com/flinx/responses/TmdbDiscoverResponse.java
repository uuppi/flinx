package com.flinx.responses;

import com.flinx.Film;

import java.util.List;

/**
 * @author Urho Laukkarinen
 */
public class TmdbDiscoverResponse
{
    private int page;
    private List<Film> results;

    public int getPage()
    {
        return page;
    }

    public List<Film> getResults()
    {
        return results;
    }
}
