package com.flinx;

import com.flinx.responses.TmdbDiscoverResponse;
import retrofit2.Call;
import retrofit2.http.GET;

/**
 * @author Urho Laukkarinen
 */
public interface TmdbService
{
    String API_KEY = "490755dce8e3ef3b321659b5480cc96a";

    @GET("discover/movie?api_key="+API_KEY)
    Call<TmdbDiscoverResponse> discover();

    @GET("configuration?api_key="+API_KEY)
    Call<TmdbConfiguration> configuration();
}
