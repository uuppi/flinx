package com.flinx;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * @author Mikko Holopainen
 */
public class TmdbDataHandler
{
    private static final String TMDB_API_URL = "https://api.themoviedb.org/3/";
    private static final String TMDB_API_KEY = "490755dce8e3ef3b321659b5480cc96a";

    private GenreList genres;


    public TmdbDataHandler()
    {
        genres = new GenreList();
    }


    public GenreList getGenres()
    {
        return genres;
    }

    public void setGenres(GenreList genres)
    {
        this.genres = genres;
    }

    public boolean loadGenresFromApi()
    {
        try
        {
            String jsonString = NetworkReader.readUrlAsString(TMDB_API_URL + "genre/movie/list?api_key=" +
                                                                      TMDB_API_KEY);

            JSONParser jsonParser = new JSONParser();
            JSONObject jsonObject = (JSONObject) jsonParser.parse(jsonString);

            JSONArray jsonGenres = (JSONArray) jsonObject.get("genres");

            GenreList genres = new GenreList();
            String id, name;

            for (Object genre : jsonGenres)
            {
                JSONObject jsonGenre = (JSONObject) genre;

                if (jsonGenre.get("id") == null || jsonGenre.get("name") == null)
                {
                    continue;
                }

                id = jsonGenre.get("id").toString();
                name = jsonGenre.get("name").toString();

                if (!id.isEmpty() && !name.isEmpty())
                {
                    genres.add(new Genre(Integer.parseInt(id), name));
                }
            }

            this.genres = genres;
            return true;
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        return false;
    }


    public void updateFilms(FilmList films)
    {
        for(Film film : films) {
            updateFilm(film);
        }
    }

    public boolean updateFilm(Film film)
    {
        Integer tmdbId = film.getTmdbId();

        if(tmdbId == null) tmdbId = findFilmIdByName(film);
        if(tmdbId == null) return false;

        return film.updateWith(readFilm(tmdbId));
    }


    public Film readFilm(int id)
    {
        try
        {
            String jsonString = NetworkReader.readUrlAsString(TMDB_API_URL + "movie/" + URLEncoder.encode(String.valueOf(id), "UTF-8") +
                                                                      "?api_key=" + TMDB_API_KEY);

            JSONParser jsonParser = new JSONParser();
            JSONObject jsonFilm = (JSONObject) jsonParser.parse(jsonString);

            int tmdbId = id;

            String titleOrig = jsonFilm.get("title") != null ? jsonFilm.get("title").toString() : "";
            String description = jsonFilm.get("overview") != null ? jsonFilm.get("overview").toString() : "";
            String imagePath = jsonFilm.get("poster_path") != null ? jsonFilm.get("poster_path").toString() : "";
            float tmdbRating = jsonFilm.get("vote_average") != null ? Float.parseFloat(jsonFilm.get("vote_average")
                                                                                               .toString()) : Float.MIN_VALUE;

            Date releaseDate = null;
            if (jsonFilm.get("release_date") != null)
            {
                releaseDate = (new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH)).parse(jsonFilm.get("release_date").toString());
            }

            ArrayList<String> directors = readFilmDirectors(id);


            List<Genre> filmGenres = new ArrayList<Genre>();
            JSONArray jsonGenres = (JSONArray) jsonFilm.get("genres");
            for (Object genre : jsonGenres)
            {
                JSONObject jsonGenre = (JSONObject) genre;
                String genreId = jsonGenre.get("id") != null ? jsonGenre.get("id").toString() : "";
                filmGenres.add(genres.getGenreByTmdbId(genreId));
            }

            if (titleOrig.isEmpty() || releaseDate == null)
            {
                return null;
            }

            return new Film(titleOrig, directors, filmGenres, description, releaseDate, imagePath, tmdbId,
                                   tmdbRating);
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        return null;
    }

    public static Integer findFilmIdByName(Film film)
    {
        return findFilmIdByName(film, true);
    }

    public static Integer findFilmIdByName(Film film, boolean searchByYear)
    {
        try
        {
            String url = TMDB_API_URL + "search/movie?query=" + URLEncoder.encode(film.getOriginalTitle(), "UTF-8") +
                                 "&api_key=" + TMDB_API_KEY;
            if (searchByYear)
            {
                url += "&year=" + URLEncoder.encode(String.valueOf(film.getProductionYear()), "UTF-8");
            }

            String jsonString = NetworkReader.readUrlAsString(url);

            JSONParser jsonParser = new JSONParser();
            JSONObject jsonObject = (JSONObject) jsonParser.parse(jsonString);

            JSONArray results = (JSONArray) jsonObject.get("results");

            String resultTitle, resultId;
            ArrayList<String> resultDirectors;

            for (Object result : results)
            {
                JSONObject jsonResult = (JSONObject) result;
                if (jsonResult.get("id") == null || jsonResult.get("original_title") == null)
                {
                    continue;
                }

                resultId = jsonResult.get("id").toString();
                resultTitle = jsonResult.get("original_title").toString();

                if (resultTitle.equals(film.getOriginalTitle()) || results.size() == 1)
                {
                    resultDirectors = readFilmDirectors(Integer.parseInt(resultId));
                    if (film.wasDirectedBy(resultDirectors))
                    {
                        return Integer.parseInt(resultId);
                    }
                }
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }

        if (searchByYear)
        {
            return findFilmIdByName(film, false);
        }

        return null;
    }

    public static ArrayList<String> readFilmDirectors(int id)
    {
        try
        {
            String jsonString = NetworkReader.readUrlAsString(TMDB_API_URL + "movie/" + URLEncoder.encode(String.valueOf(id), "UTF-8") +
                                                                      "/credits?api_key=" + TMDB_API_KEY);

            JSONParser jsonParser = new JSONParser();
            JSONObject jsonObject = (JSONObject) jsonParser.parse(jsonString);

            JSONArray crew = (JSONArray) jsonObject.get("crew");

            ArrayList<String> directors = new ArrayList<>();

            for (Object crewmember : crew)
            {
                JSONObject jsonCrewmember = (JSONObject) crewmember;
                if (jsonCrewmember.get("job") != null && (jsonCrewmember.get("job").toString()).equals("Director"))
                {
                    directors.add(jsonCrewmember.get("name").toString());
                }
            }

            return directors;
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        return null;
    }


    public FilmList discover(DiscoverBy discoverBy)
    {
        return discover(discoverBy, null);
    }

    public FilmList discover(DiscoverBy discoverBy, Genre genre)
    {
        try
        {
            String url = TMDB_API_URL + "discover/movie?api_key=" + TMDB_API_KEY;

            if (genre != null)
            {
                url += "&with_genres=" + genre.getId();
            }

            switch (discoverBy)
            {
                case POPULARITY:
                    url += "&sort_by=popularity.desc";
                    break;
                case PRODUCTION_YEAR_OLDEST:
                    url += "&sort_by=release_date.asc&vote_count.gte=10";
                    break;
                case PRODUCTION_YEAR_NEWEST:
                    url += "&sort_by=release_date.desc&vote_count.gte=10";
                    break;
                case REVENUE:
                    url += "&sort_by=revenue.desc";
                    break;
                case VOTE_AVERAGE_BEST:
                    url += "&sort_by=vote_average.desc&vote_count.gte=500";
                    break;
                case VOTE_AVERAGE_WORST:
                    url += "&sort_by=vote_average.asc&vote_count.gte=50";
                    break;
            }

            String jsonString = NetworkReader.readUrlAsString(url);

            JSONParser jsonParser = new JSONParser();
            JSONObject jsonObject = (JSONObject) jsonParser.parse(jsonString);

            JSONArray results = (JSONArray) jsonObject.get("results");

            FilmList films = new FilmList();

            for (Object result : results)
            {
                JSONObject jsonFilm = (JSONObject) result;

                if (jsonFilm.get("id") == null || jsonFilm.get("original_title") == null || jsonFilm.get("release_date") == null)

                {
                    continue;
                }

                String tmdbId = jsonFilm.get("id") != null ? jsonFilm.get("id").toString() : "";
                String titleOrig = jsonFilm.get("title") != null ? jsonFilm.get("title").toString() : "";
                String description = jsonFilm.get("overview") != null ? jsonFilm.get("overview").toString() : "";
                String posterPath = jsonFilm.get("poster_path") != null ? jsonFilm.get("poster_path").toString() : "";
                String backdropPath = jsonFilm.get("backdrop_path") != null ? jsonFilm.get("backdrop_path").toString() : "";
                float tmdbRating = jsonFilm.get("vote_average") != null ? Float.parseFloat(jsonFilm.get("vote_average").toString()) : Float.MIN_VALUE;

                Date releaseDate = null;
                if (jsonFilm.get("release_date") != null)
                {
                    releaseDate = (new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH)).parse(jsonFilm.get("release_date").toString());
                }

                List<Genre> filmGenres = new ArrayList<>();
                JSONArray genreIds = (JSONArray) jsonFilm.get("genre_ids");
                for (Object objGenreId : genreIds)
                {
                    String genreId = Long.toString(((long) objGenreId));
                    filmGenres.add(genres.getGenreByTmdbId(genreId));
                }

                films.add(new Film(titleOrig, filmGenres, description, releaseDate, posterPath, backdropPath, tmdbId, tmdbRating));
            }

            return films;
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        return null;
    }
}
