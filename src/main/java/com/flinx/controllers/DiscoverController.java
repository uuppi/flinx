package com.flinx.controllers;

import com.flinx.Film;
import com.flinx.TmdbService;
import com.flinx.dao.FilmDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

import java.io.IOException;
import java.util.List;

import static org.springframework.web.bind.annotation.RequestMethod.GET;

/**
 * @author Urho Laukkarinen
 */
@Controller
@RequestMapping("/discover")
public class DiscoverController
{
    @Autowired
    private TmdbService tmdbService;

    @Autowired
    private FilmDAO filmDAO;

    @RequestMapping(method = GET)
    public String discover()
    {
        return "discover";
    }

    @ModelAttribute("films")
    public List<Film> films() throws IOException
    {
        System.out.println("DiscoverController:films()");

        return filmDAO.getFilmsOrderByRating();
    }
}
