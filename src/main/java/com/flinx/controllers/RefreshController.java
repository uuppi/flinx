package com.flinx.controllers;

import com.flinx.*;
import com.flinx.dao.CinemaDAO;
import com.flinx.dao.FilmDAO;
import com.flinx.responses.TmdbDiscoverResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import retrofit2.Response;

import java.io.IOException;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;
import static org.springframework.web.bind.annotation.RequestMethod.GET;

/**
 * @author Urho Laukkarinen
 */
@Controller
@RequestMapping("/refresh")
public class RefreshController
{
    @Autowired
    private ApplicationContext context;

    @Autowired
    private TmdbService tmdbService;

    @RequestMapping(method = GET)
    public String refresh()
    {
        return "refresh";
    }

    @RequestMapping(value = "/{type}", method = GET, produces = {APPLICATION_JSON_VALUE})
    @ResponseBody
    public String refreshData(@PathVariable String type) throws InterruptedException, IOException
    {
        boolean success = false;
        switch (type)
        {
            case "tmdb_films":
                success = refreshAll();
                break;
/*
            case "cinemas":
                success = refreshCinemas();
                break;

            case "cinema_films":
                success = refreshCinemaFilms();
                break;*/
        }

        System.out.println("refreshing " + type);

        return "{\"success\" : "+success+"}";
    }

    private boolean refreshAll()
    {
        FilmDAO filmDAO = context.getBean(FilmDAO.class);
        CinemaDAO cinemaDAO = context.getBean(CinemaDAO.class);

        filmDAO.deleteAll();
        cinemaDAO.deleteAll();

        DataHandler dh = new DataHandler();

        System.out.println("Loading data from APIs ...");
        dh.loadDataFromApis();

        FilmList films = dh.discover(DiscoverBy.VOTE_AVERAGE_BEST);
        dh.updateNetflixStatus(films);

        /*
        for(Genre genre : dh.getTmdbDataHandler().getGenres())
        {
            FilmList genreFilms = dh.discover(DiscoverBy.POPULARITY, genre);

            dh.updateNetflixStatus(genreFilms);
            System.out.println("-----" + genre.getName() + "-----");
            System.out.println(genreFilms);
        }*/

        //System.out.println("Updating cinemas ...");
        //dh.updateCinemas();

        filmDAO.save(films.getFilms());
        cinemaDAO.save(dh.getCinemaDataHandler().getCinemas());

        return true;
    }

    private boolean refreshCinemas()
    {
        CinemaDataHandler cinemaDataHandler = new CinemaDataHandler();

        if (!cinemaDataHandler.readCinemasFromApi())
        {
            return false;
        }

        CinemaDAO cinemaDAO = context.getBean(CinemaDAO.class);
        cinemaDAO.deleteAll();

        cinemaDAO.save(cinemaDataHandler.getCinemas());

        return true;
    }

    private boolean refreshTmdbFilms() throws IOException
    {
        FilmDAO filmDAO = context.getBean(FilmDAO.class);
        filmDAO.deleteAll();

        TmdbDataHandler tmdbDataHandler = new TmdbDataHandler();

        /*
        FilmList filmList = tmdbDataHandler.discover(DiscoverBy.POPULARITY);

        if (filmList != null)
        {
            filmDAO.save(filmList.getFilmList());
            return true;
        }

        return false;
        */


        Response<TmdbDiscoverResponse> response = tmdbService.discover().execute();

        if (response.isSuccessful())
        {
            filmDAO.save(response.body().getResults());
            return true;
        }

        return true;
    }
}
