package com.flinx.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestMapping;

import static org.springframework.web.bind.annotation.RequestMethod.GET;

/**
 * @author Urho Laukkarinen
 */
@Controller
@RequestMapping("/error")
public class ErrorController
{
    @ExceptionHandler
    @RequestMapping(method = GET)
    public String error()
    {
        return "error";
    }
}
