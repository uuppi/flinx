package com.flinx.controllers;

import com.flinx.Film;
import com.flinx.dao.FilmDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;

import static org.springframework.web.bind.annotation.RequestMethod.GET;

/**
 * @author Urho Laukkarinen
 */
@Controller
@RequestMapping("/films")
public class FilmsController
{
    @Autowired
    private FilmDAO filmDAO;

    @RequestMapping(value = "/{slug}", method = GET)
    public String getFilm(@PathVariable String slug, Model model)
    {
        Film film = filmDAO.getFilmBySlug(slug);

        if (film == null)
        {
            throw new FilmNotFoundException();
        }

        model.addAttribute("film", film);

        return "film";
    }

    @ResponseStatus(value = HttpStatus.NOT_FOUND)
    public class FilmNotFoundException extends RuntimeException
    {

    }
}
