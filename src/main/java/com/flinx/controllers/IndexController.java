package com.flinx.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import static org.springframework.web.bind.annotation.RequestMethod.GET;


/**
 *
 * @author Urho Laukkarinen
 */
@Controller
@RequestMapping("/")
public class IndexController
{
    @RequestMapping(method = GET)
    public String index()
    {
        return "redirect:/discover";
    }
}
