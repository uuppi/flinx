package com.flinx;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 *
 * @author Mikko Holopainen
 */
public class CinemaDataHandler
{
    private static final String CINEMA_AREAS_URL = "http://www.finnkino.fi/xml/TheatreAreas/";
    private static final String CINEMA_EVENTS_URL = "http://www.finnkino.fi/xml/Events/";
    private static final String CINEMA_SCHEDULE_URL = "http://www.finnkino.fi/xml/Schedule/";
    private static Pattern CINEMA_TITLE_PATTERN = Pattern.compile("^(.*?)(?:\\(.*?\\)|3D|2D|-|\\s)*$");
    
    private ArrayList<Cinema> cinemas;

    private FilmList filmsInCinemas;
    
    
    public CinemaDataHandler()
    {
        cinemas = new ArrayList<>();
    }
    
    public boolean readCinemasFromApi()
    {
        ArrayList<Cinema> cinemas = new ArrayList<>();
        try
        {	
            String xmlString = NetworkReader.readUrlAsString(CINEMA_AREAS_URL);
            
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            
            Document xmlDocument = dBuilder.parse(new InputSource(new StringReader(xmlString)));
            xmlDocument.getDocumentElement().normalize();
            
            NodeList cinemaNodes = xmlDocument.getElementsByTagName("TheatreArea");
            
            String cinemaId, name;
                    
            for (int i = 0; i < cinemaNodes.getLength(); i++)
            {

                Node cinemaNode = cinemaNodes.item(i);

                if (cinemaNode.getNodeType() == Node.ELEMENT_NODE)
                {
                    Element cinemaElement = (Element) cinemaNode;

                    cinemaId = getChildContent(cinemaElement, "ID");
                    name = getChildContent(cinemaElement, "Name");
                    
                    if(cinemaId == null || cinemaId.equals("") || name == null || name.equals("")) continue;
                    if(name.equals("Valitse alue/teatteri") || name.equals("Pääkaupunkiseutu")) continue; // Turhien poisto
                    
                    cinemas.add(new Cinema(Integer.parseInt(cinemaId), name));
                }
            }
            
            this.cinemas = cinemas;
            return true;
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        
        return false;
    }

    public List<Cinema> getCinemas()
    {
        return cinemas;
    }
    
    
    private String parseTitle(String cinemaTitle)
    {
        Matcher matcher = CINEMA_TITLE_PATTERN.matcher(cinemaTitle);
        if (matcher.find()) return matcher.group(1);
        else return "";
    }
    
    private Element getChildElement(Element element, String name)
    {
        if(element == null) return null;
        NodeList children = element.getElementsByTagName(name);
        if(children.getLength() > 0)
        {
            Node childNode = children.item(0);
            if (childNode.getNodeType() == Node.ELEMENT_NODE) return (Element) childNode;
        }
        return null;
    }
    
    private ArrayList<Element> getChildElements(Element element, String name)
    {
        if(element == null) return null;
        
        ArrayList<Element> childElements = new ArrayList<>();
        NodeList children = element.getElementsByTagName(name);
        
        for (int i = 0; i < children.getLength(); i++)
        {
            Node childNode = children.item(i);
            if (childNode.getNodeType() == Node.ELEMENT_NODE) childElements.add((Element) childNode);
        }
        
        return childElements;
    }
    
    private String getChildContent(Element element, String name)
    {
        if(element == null) return "";
        Element childElement = getChildElement(element, name);
        if(childElement != null) return childElement.getTextContent();
        return "";
    }
    
    
    public FilmList loadFilmsInCinemas()
    {
        filmsInCinemas = new FilmList();
        try
        {	
            String xmlString = NetworkReader.readUrlAsString(CINEMA_EVENTS_URL);
            
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            
            Document xmlDocument = dBuilder.parse(new InputSource(new StringReader(xmlString)));
            xmlDocument.getDocumentElement().normalize();
            
            NodeList eventNodes = xmlDocument.getElementsByTagName("Event");
            
            String nameOrig, nameFi, productionYear, cinemaId, cinemaUrl, director;
            ArrayList<String> directors;
  
            for (int i = 0; i < eventNodes.getLength(); i++)
            {
                Node eventNode = eventNodes.item(i);
                
                if (eventNode.getNodeType() == Node.ELEMENT_NODE)
                {
                    Element eventElement = (Element) eventNode;

                    nameOrig = parseTitle(getChildContent(eventElement, "OriginalTitle"));
                    productionYear = parseTitle(getChildContent(eventElement, "ProductionYear"));
                    cinemaId = getChildContent(eventElement, "ID");
                    cinemaUrl = getChildContent(eventElement, "EventURL");
                    
                    ArrayList<Element> directorElements = getChildElements(getChildElement(eventElement, "Directors"), "Director");
                    directors = new ArrayList<>();
                    for(Element directorElement : directorElements)
                    {
                        director = getChildContent(directorElement, "FirstName") + " " + getChildContent(directorElement, "LastName");
                        if(!director.trim().equals("")) directors.add(director);
                    }
                    
                    if( nameOrig == null || nameOrig.equals("") || productionYear == null || productionYear.equals("") ||
                        cinemaId == null || cinemaId.equals("") || cinemaUrl == null || cinemaUrl.equals("") ||
                        directors.isEmpty())
                            continue;

                    Calendar calendar = Calendar.getInstance();
                    calendar.set(Calendar.YEAR, Integer.parseInt(productionYear));
                    
                    filmsInCinemas.addCinemaFilm(new Film(nameOrig, calendar.getTime(), directors, Integer.parseInt(cinemaId), cinemaUrl));
                }
            }
        }
        catch (Exception e)
        {
            return null;
        }
        
        return filmsInCinemas;
    }
    
    public void updateCinemas(FilmList filmsInCinemas)
    {
        for(Cinema cinema : cinemas)
        {
            updateCinemaProgram(filmsInCinemas, cinema);
        }
    }

    public boolean updateCinemaProgram(FilmList films, Cinema cinema)
    {
        try
        {	
            String xmlString = NetworkReader.readUrlAsString(CINEMA_SCHEDULE_URL + "?area=" + cinema.getId());
            
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            
            Document xmlDocument = dBuilder.parse(new InputSource(new StringReader(xmlString)));
            xmlDocument.getDocumentElement().normalize();
            
            Element showsElement = getChildElement(xmlDocument.getDocumentElement(), "Shows");
            if(showsElement == null) return false;
            
            NodeList showNodes = showsElement.getElementsByTagName("Show");
                    
            String cinemaId;
            
            for (int i = 0; i < showNodes.getLength(); i++)
            {
                Node showNode = showNodes.item(i);
                if (showNode.getNodeType() == Node.ELEMENT_NODE)
                {
                    Element showElement = (Element) showNode;

                    cinemaId = getChildContent(showElement, "EventID");
                    
                    if(cinemaId.equals("")) continue;

                    cinema.addFilm(films.getFilmByCinemaId(Integer.parseInt(cinemaId)));
                }
            }
        }
        catch (Exception e)
        {
            return false;
        }
        
        return true;
    }
}
