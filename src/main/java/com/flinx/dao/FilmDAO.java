package com.flinx.dao;

import com.flinx.Film;
import org.hibernate.Session;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import java.util.List;

/**
 * @author Urho Laukkarinen
 */
public final class FilmDAO extends BaseDAO<Film>
{
    @Override
    public List<Film> list()
    {
        try (Session session = openSession())
        {
            return session.createCriteria(Film.class).list();
        }
    }

    @Override
    public void save(Film film)
    {
        // Update slug
        film.setTitleSlug(null);

        super.save(film);
    }

    @Override
    public void save(List<Film> list)
    {
        // Update slugs
        for (Film film : list)
        {
            film.setTitleSlug(null);
        }

        super.save(list);
    }

    public Film getFilmById(int id)
    {
        try(Session session = openSession())
        {
            return (Film) session.createCriteria(Film.class)
                                  .add(Restrictions.eq("id", id))
                                  .uniqueResult();
        }
    }

    public Film getFilmBySlug(String slug)
    {
        try(Session session = openSession())
        {
            return (Film) session.createCriteria(Film.class)
                                  .add(Restrictions.eq("titleSlug", slug))
                                  .uniqueResult();
        }
    }

    public List<Film> getFilmsOrderByRating()
    {
        try(Session session = openSession())
        {
            return session.createCriteria(Film.class)
                    .addOrder(Order.desc("tmdbRating"))
                    .list();
        }
    }
}
