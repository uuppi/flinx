package com.flinx.dao;

import com.flinx.Cinema;
import org.hibernate.Session;

import java.util.List;

/**
 * @author Urho Laukkarinen
 */
public class CinemaDAO extends BaseDAO<Cinema>
{
    @Override
    public List<Cinema> list()
    {
        try (Session session = openSession())
        {
            return session.createCriteria(Cinema.class).list();
        }
    }
}
