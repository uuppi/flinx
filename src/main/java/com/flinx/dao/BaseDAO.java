package com.flinx.dao;


import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.StatelessSession;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

/**
 * @author Urho Laukkarinen
 */
public abstract class BaseDAO<T>
{
    @Autowired
    private SessionFactory sessionFactory;

    public void setSessionFactory(SessionFactory sessionFactory)
    {
        this.sessionFactory = sessionFactory;
    }

    protected SessionFactory getSessionFactory()
    {
        return sessionFactory;
    }

    public void saveOrUpdate(T obj)
    {
        getSessionFactory().getCurrentSession().saveOrUpdate(obj);
    }

    public void save(T obj)
    {
        Session session = openSession();
        Transaction transaction = session.beginTransaction();

        session.persist(obj);

        transaction.commit();
        session.close();
    }

    public void save(List<T> list)
    {
        StatelessSession session = sessionFactory.openStatelessSession();
        Transaction transaction = session.beginTransaction();

        list.forEach(session::insert);

        transaction.commit();
        session.close();
    }

    public abstract List<T> list();

    public void deleteAll()
    {
        Session session = openSession();

        Transaction transaction = session.beginTransaction();

        list().forEach(session::delete);

        transaction.commit();
        session.close();
    }

    public Session openSession()
    {
        return getSessionFactory().openSession();
    }
}
