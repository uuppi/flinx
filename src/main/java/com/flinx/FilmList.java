package com.flinx;

import java.util.ArrayList;
import java.util.Iterator;

/**
 *
 * @author Mikko Holopainen
 */
public class FilmList implements Iterable<Film>
{
    ArrayList<Film> films;

    public FilmList()
    {
        films = new ArrayList<>();
    }
    
    public void add(Film newFilm)
    {
        if(newFilm == null) return;
        
        for(Film film : films)
        {
            if(film.equals(newFilm))
            {
                film.updateWith(newFilm);
                return;
            }
        }
        
        films.add(newFilm);
    }
    
    public void add(FilmList newFilms)
    {
        for(Film newFilm : newFilms) add(newFilm);
    }
    
    public void add(ArrayList<Film> newFilms)
    {
        for(Film newFilm : newFilms) add(newFilm);
    }
    
    public boolean addCinemaFilm(Film newFilm)
    {
        for(Film film : films)
        {
            if(film.equals(newFilm)) return false;
            if(film.getOriginalTitle().equals(newFilm.getOriginalTitle()) && film.hasSameDirectorsAs(newFilm))
            {
                film.addCinemaId(newFilm.getCinemaIds());
                return true;
            }
        }

        films.add(newFilm);
        return true;
    }

    public Film getFilmByCinemaId(int cinemaId)
    {
        for(Film film : films) {
            if(film.getCinemaIds().contains(cinemaId))
            {
                return film;
            }
        }
        
        return null;
    }

    public ArrayList<Film> getFilms()
    {
        return films;
    }

    public void setFilms(ArrayList<Film> films)
    {
        this.films = films;
    }
    
    @Override
    public Iterator<Film> iterator() {
        return films.iterator();
    }
    
    @Override
    public String toString()
    {
        String output = "";
        for(Film film : films)
        {
            output += film + "\n";
        }
        return output;
    }
}
