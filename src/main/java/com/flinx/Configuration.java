package com.flinx;

import com.flinx.dao.CinemaDAO;
import com.flinx.dao.FilmDAO;
import okhttp3.OkHttpClient;
import org.hibernate.SessionFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.orm.hibernate5.LocalSessionFactoryBuilder;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import javax.sql.DataSource;

/**
 * @author Urho Laukkarinen
 */
@org.springframework.context.annotation.Configuration
public class Configuration
{
    @Bean
    public OkHttpClient okHttpClient()
    {
        return new OkHttpClient.Builder()
                       .addInterceptor(new LoggingInterceptor())
                       .build();
    }

    @Bean
    public Retrofit tmdbRetrofit(OkHttpClient okHttpClient)
    {
        return new Retrofit.Builder()
                       .baseUrl("https://api.themoviedb.org/3/")
                       .addConverterFactory(GsonConverterFactory.create())
                       .client(okHttpClient)
                       .build();
    }

    @Bean
    public TmdbService tmdbService(Retrofit tmdbRetrofit)
    {
        return tmdbRetrofit.create(TmdbService.class);
    }

    @Bean
    public SessionFactory sessionFactory(DataSource dataSource)
    {
        SessionFactory sessionFactory = new LocalSessionFactoryBuilder(null)
                                                .configure("hibernate.cfg.xml")
                                                .buildSessionFactory();

        sessionFactory.openSession();

        return sessionFactory;
    }

    @Bean
    public DataSource dataSource() throws ClassNotFoundException
    {
        Class.forName("com.mysql.jdbc.Driver");

        DriverManagerDataSource dataSource = new DriverManagerDataSource();
        dataSource.setDriverClassName("com.mysql.jdbc.Driver");
        dataSource.setUrl("jdbc:mysql://localhost:3306/flinx");
        dataSource.setUsername("root");
        dataSource.setPassword("root66");

        return dataSource;
    }

    @Bean
    public FilmDAO filmDAO()
    {
        return new FilmDAO();
    }

    @Bean
    public CinemaDAO cinemaDAO()
    {
        return new CinemaDAO();
    }
}
