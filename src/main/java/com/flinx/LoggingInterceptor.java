package com.flinx;

import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;

import java.io.IOException;

import static okhttp3.internal.Internal.logger;

/**
 * @author Urho Laukkarinen
 */
public class LoggingInterceptor implements Interceptor
{
    @Override
    public Response intercept(Chain chain) throws IOException
    {
        Request request = chain.request();

        long time1 = System.nanoTime();

        logger.info(String.format("Sending request %s on %s%n%s", request.url(), chain.connection(), request.headers()));

        Response response = chain.proceed(request);

        long time2 = System.nanoTime();

        logger.info(String.format("Received response %s in %.1fms%n%s", response.request().url(), (time2-time1) / 1e6d, response.headers()));

        return response;
    }
}
