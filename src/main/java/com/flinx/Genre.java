package com.flinx;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 *
 * @author Mikko Holopainen
 */
@Entity
@Table(name="genre")
public class Genre
{
    @Id
    private int id;
    private String name;

    public Genre(int id, String name)
    {
        this.id = id;
        this.name = name;
    }

    public Genre()
    {

    }

    public int getId()
    {
        return id;
    }

    public void setId(int id)
    {
        this.id = id;
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }
}
