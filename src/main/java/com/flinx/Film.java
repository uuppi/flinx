package com.flinx;

import com.google.gson.annotations.SerializedName;
import de.weltraumschaf.speakingurl.Slug;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * @author Mikko Holopainen
 */
@Entity
@Table(name = "film")
@Access(AccessType.FIELD)
public class Film
{
    @Id
    @SerializedName("id")
    @Column(name = "id")
    private Integer tmdbId;
    @SerializedName("original_title")
    @Column(name = "name_original")
    private String titleOrig;
    @Column(name="name_slug")
    private String titleSlug;
    @SerializedName("overview")
    @Column(name = "description")
    private String description;
    @SerializedName("release_date")
    @Column(name = "release_date")
    private Date releaseDate;
    @SerializedName("poster_path")
    @Column(name = "poster_path")
    private String posterPath;
    @SerializedName("backdrop_path")
    @Column(name = "backdrop_path")
    private String backdropPath;
    @Column(name = "netflix_id")
    private String netflixId;
    @Column(name = "netflix_checked")
    private Date netflixChecked;
    @Column(name = "cinema_url")
    private String cinemaUrl;
    @Column(name = "tmdb_rating")
    private float tmdbRating;

    @Transient
    private List<String> directors;

    @Transient
    private List<Integer> cinemaIds;

    @Transient
    private List<Genre> genres;


    public Film()
    {
        cinemaIds = new ArrayList<>();
        directors = new ArrayList<>();
        genres = new ArrayList<>();
        tmdbRating = Float.MIN_VALUE;
        netflixChecked = new Date(0);
    }

    // Elokuvan luominen TMDb-APIn tiedoista ilman ohjaajia
    public Film(String titleOrig, List<Genre> genres, String description, Date releaseDate, String posterPath, String backdropPath,
                String tmdbId, float tmdbRating)
    {
        this();

        this.titleOrig = titleOrig;
        this.genres = genres;
        this.description = description;
        this.releaseDate = releaseDate;
        this.posterPath = posterPath;
        this.backdropPath = backdropPath;
        this.tmdbId = Integer.parseInt(tmdbId);
        this.tmdbRating = tmdbRating;
    }

    // Elokuvan luominen TMDb-APIn tiedoista
    public Film(String titleOrig, ArrayList<String> directors, List<Genre> genres, String description, Date releaseDate, String posterPath, Integer tmdbId, float tmdbRating)
    {
        this();

        this.titleOrig = titleOrig;
        this.directors = directors;
        this.genres = genres;
        this.description = description;
        this.releaseDate = releaseDate;
        this.posterPath = posterPath;
        this.tmdbId = tmdbId;
        this.tmdbRating = tmdbRating;
    }

    // Elokuvan luominen Finnkino-APIn tiedoista
    public Film(String titleOrig, Date releaseDate, ArrayList<String> directors, Integer cinemaId, String cinemaUrl)
    {
        this();

        this.titleOrig = titleOrig;
        this.releaseDate = releaseDate;
        this.directors = directors;
        cinemaIds.add(cinemaId);
        this.cinemaUrl = cinemaUrl;
    }

    public boolean wasDirectedBy(ArrayList<String> thoseDirectors)
    {
        int found = 0;
        for (String director : directors)
        {
            if (thoseDirectors.contains(director))
            {
                found++;
            }
        }

        return found > 0;
    }

    public boolean hasSameDirectorsAs(Film thatFilm)
    {
        ArrayList<String> thoseDirectors = (ArrayList) thatFilm.getDirectors();
        return wasDirectedBy(thoseDirectors);
    }

    public boolean equals(Film thatFilm)
    {
        if (tmdbId != null && tmdbId == thatFilm.getTmdbId())
        {
            return true;
        }

        if ((netflixId != null && netflixId.equals(thatFilm.getNetflixId())) ||
                    (cinemaUrl != null && cinemaUrl.equals(thatFilm.getCinemaUrl())))
        {
            return true;
        }

        if (titleOrig != null && titleOrig.equals(thatFilm.getOriginalTitle()) &&
                    releaseDate != null && releaseDate.equals(thatFilm.getReleaseDate()) &&
                    hasSameDirectorsAs(thatFilm))
        {
            return true;
        }

        for (int thisCinemaId : cinemaIds)
        {
            for (int thatCinemaId : thatFilm.getCinemaIds())
            {
                if (thisCinemaId == thatCinemaId)
                {
                    return true;
                }
            }
        }

        return false;
    }

    public boolean updateWith(Film film)
    {
        if (film == null)
        {
            return false;
        }

        if (film.getTmdbId() != null)
        {
            tmdbId = film.getTmdbId();
        }

        if (film.getOriginalTitle() != null && !film.getOriginalTitle().isEmpty())
        {
            titleOrig = film.getOriginalTitle();
        }
        if (film.getDescription() != null && !film.getDescription().isEmpty())
        {
            description = film.getDescription();
        }
        if (film.getReleaseDate() != null)
        {
            releaseDate = film.getReleaseDate();
        }
        if (film.getPosterPath() != null && !film.getPosterPath().isEmpty())
        {
            posterPath = film.getPosterPath();
        }
        if (film.getCinemaUrl() != null && !film.getCinemaUrl().isEmpty())
        {
            cinemaUrl = film.getCinemaUrl();
        }
        if (film.getTmdbRating() >= 0)
        {
            tmdbRating = film.getTmdbRating();
        }

        if (!film.getGenres().isEmpty())
        {
            for (Genre genre : film.getGenres())
            {
                if (!genres.contains(genre))
                {
                    genres.add(genre);
                }
            }
        }

        if (!film.getDirectors().isEmpty())
        {
            for (String director : film.getDirectors())
            {
                if (!directors.contains(director))
                {
                    directors.add(director);
                }
            }
        }

        if (!film.getCinemaIds().isEmpty())
        {
            for (int cinemaId : film.getCinemaIds())
            {
                if (!cinemaIds.contains(cinemaId))
                {
                    cinemaIds.add(cinemaId);
                }
            }
        }

        if (film.getNetflixChecked().compareTo(netflixChecked) > 0)
        {
            netflixChecked = film.getNetflixChecked();
            netflixId = film.getNetflixId();
        }

        return true;
    }

    public void updateNetflixId(String netflixId)
    {
        this.netflixId = netflixId;
        netflixChecked = new Date();
    }


    public Integer getTmdbId()
    {
        return tmdbId;
    }

    public void setTmdbId(Integer id)
    {
        this.tmdbId = id;
    }

    public String getOriginalTitle()
    {
        return titleOrig;
    }

    public void setOriginalTitle(String titleOrig)
    {
        this.titleOrig = titleOrig;
        getTitleSlug();
    }

    public String getDescription()
    {
        return description;
    }

    public void setDescription(String description)
    {
        this.description = description;
    }

    public Date getReleaseDate()
    {
        return releaseDate;
    }

    public void setReleaseDate(Date releaseDate)
    {
        this.releaseDate = releaseDate;
    }

    public String getPosterPath()
    {
        return posterPath;
    }

    public void setPosterPath(String posterPath)
    {
        this.posterPath = posterPath;
    }

    public String getNetflixId()
    {
        return netflixId;
    }

    public String getCinemaUrl()
    {
        return cinemaUrl;
    }

    public void setCinemaUrl(String cinemaUrl)
    {
        this.cinemaUrl = cinemaUrl;
    }

    public List<Integer> getCinemaIds()
    {
        return cinemaIds;
    }

    public void addCinemaId(int newCinemaId)
    {
        if (!cinemaIds.contains(newCinemaId))
        {
            cinemaIds.add(newCinemaId);
        }
    }

    public void addCinemaId(List<Integer> newCinemaIds)
    {
        for (int newCinemaId : newCinemaIds)
        {
            if (!cinemaIds.contains(newCinemaId))
            {
                cinemaIds.add(newCinemaId);
            }
        }
    }

    public void setCinemaIds(List<Integer> cinemaIds)
    {
        this.cinemaIds = cinemaIds;
    }

    public float getTmdbRating()
    {
        return tmdbRating;
    }

    public void setTmdbRating(float tmdbRating)
    {
        this.tmdbRating = tmdbRating;
    }

    public Date getNetflixChecked()
    {
        return netflixChecked;
    }

    public void setNetflixChecked(Date netflixChecked)
    {
        this.netflixChecked = netflixChecked;
    }

    public List<String> getDirectors()
    {
        return directors;
    }

    public void setDirectors(ArrayList<String> directors)
    {
        this.directors = directors;
    }

    public List<Genre> getGenres()
    {
        return genres;
    }

    public void setGenres(List<Genre> genres)
    {
        this.genres = genres;
    }

    public boolean hasNetflixId()
    {
        return netflixId != null && !netflixId.isEmpty();
    }

    public boolean hasTitleAndDirector()
    {
        return titleOrig != null && !titleOrig.isEmpty() && directors.size() > 0;
    }

    @Override
    public String toString()
    {
        String out = titleOrig + " (" + releaseDate + ")";
        if (netflixId != null && !netflixId.isEmpty())
        {
            out += " [N]";
        }
        out += ": ";
        for (Genre genre : genres)
        {
            out += genre.getName() + ", ";
        }
        return out;
    }

    public String getBackdropPath()
    {
        return backdropPath;
    }

    public void setTitleSlug(String slug)
    {
        if (slug != null)
        {
            titleSlug = slug;
        }
        else
        {
            final Slug slugger = Slug.Builder.newBuiler().create();

            titleSlug = slugger.get(titleOrig + "-" + getProductionYear());
        }
    }

    public String getTitleSlug()
    {
        return titleSlug;
    }

    public int getProductionYear()
    {
        if (releaseDate == null)
        {
            return -1;
        }

        Calendar calendar = Calendar.getInstance();
        calendar.setTime(releaseDate);
        return calendar.get(Calendar.YEAR);
    }

    public void setBackdropPath(String backgropPath)
    {
        this.backdropPath = backgropPath;
    }
}
