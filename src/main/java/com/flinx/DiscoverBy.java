package com.flinx;

/**
 *
 * @author Mikko Holopainen
 */
public enum DiscoverBy
{
    POPULARITY,
    PRODUCTION_YEAR_OLDEST,
    PRODUCTION_YEAR_NEWEST,
    REVENUE,
    VOTE_AVERAGE_BEST,
    VOTE_AVERAGE_WORST
}
