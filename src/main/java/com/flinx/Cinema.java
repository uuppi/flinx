package com.flinx;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Mikko Holopainen
 */
@Entity
@Table(name="cinema")
public class Cinema
{
    @Id
    private int id;
    private String name;
    @ManyToMany
    private List<Film> films;

    public Cinema()
    {
        this.films = new ArrayList<>();
    }

    public Cinema(int id, String name)
    {
        this();
        this.id = id;
        this.name = name;
    }

    public void addFilm(Film film)
    {
        films.add(film);
    }

    public int getId()
    {
        return id;
    }

    public void setId(int id)
    {
        this.id = id;
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public List<Film> getFilms()
    {
        return films;
    }

    public void setFilms(List<Film> films)
    {
        this.films = films;
    }
    
    @Override
    public String toString()
    {
        return name + ": " + films;
    }
}
