package com.flinx;

import java.util.ArrayList;
import java.util.Iterator;

/**
 *
 * @author Mikko Holopainen
 */
public class GenreList  implements Iterable<Genre>
{
    ArrayList<Genre> genres;


    public GenreList()
    {
        genres = new ArrayList<>();
    }
    
    public GenreList(ArrayList<Genre> genres)
    {
        this.genres = genres;
    }
    
    public void add(Genre newGenre)
    {
        if(newGenre == null || genres.contains(newGenre)) return;
        genres.add(newGenre);
    }
    
    public void add(ArrayList<Genre> newGenres)
    {
        for(Genre newGenre : newGenres) add(newGenre);
    }
    
    public Genre getGenreByTmdbId(String tmdbId)
    {
        if(tmdbId == null || tmdbId.isEmpty()) return null;
        
        for(Genre genre : genres) {
            if(Integer.parseInt(tmdbId) == genre.getId()) return genre;
        }
        
        return null;
    }
    
    public Genre getGenreByName(String name)
    {
        if(name == null || name.isEmpty()) return null;
        
        for(Genre genre : genres) {
            if(genre.getName().contains(name)) return genre;
        }
        
        return null;
    }

    public ArrayList<Genre> getGenres()
    {
        return genres;
    }

    public void setGenres(ArrayList<Genre> genres)
    {
        this.genres = genres;
    }
    
    @Override
    public Iterator<Genre> iterator() {
        return genres.iterator();
    }
}
