package com.flinx;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * @author Urho Laukkarinen
 */

public class TmdbConfiguration
{
    @SerializedName("images")
    public Images images;

    public class Images
    {
        @SerializedName("base_url")
        private String baseUrl;

        @SerializedName("secure_base_url")
        private String secureBaseUrl;

        @SerializedName("backgrop_sizes")
        private List<String> backdropSizes;

        @SerializedName("logo_sizes")
        private List<String> logoSizes;

        @SerializedName("poster_sizes")
        private List<String> posterSizes;

        @SerializedName("profile_sizes")
        private List<String> profileSizes;

        @SerializedName("still_sizes")
        private List<String> stillSizes;

        public String getBaseUrl()
        {
            return baseUrl;
        }

        public String getSecureBaseUrl()
        {
            return secureBaseUrl;
        }

        public List<String> getBackdropSizes()
        {
            return backdropSizes;
        }

        public List<String> getLogoSizes()
        {
            return logoSizes;
        }

        public List<String> getPosterSizes()
        {
            return posterSizes;
        }

        public List<String> getProfileSizes()
        {
            return profileSizes;
        }

        public List<String> getStillSizes()
        {
            return stillSizes;
        }
    }
}
