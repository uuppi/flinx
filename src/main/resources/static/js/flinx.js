$(function() {
    $(".image img").lazyload({
        threshold: 700,
        effect: "fadeIn"
    });

    $(".btn-netflix").click(function(){
        document.location = 'https://www.netflix.com/watch/'+$(this).attr("data-id");
    });
});